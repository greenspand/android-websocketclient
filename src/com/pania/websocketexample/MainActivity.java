package com.pania.websocketexample;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import de.tavendo.autobahn.WebSocketConnection;
import de.tavendo.autobahn.WebSocketException;
import de.tavendo.autobahn.WebSocketHandler;

/**
 * This Activity shows a very basic application for the use of the Autobahn
 * Android library. You can send a text via an EditText to a WebSocket Server.
 * Any received messages (even by other Clients) will be displayed next to
 * 'Received:'. Remember to add '<uses-permission
 * android:name="android.permission.INTERNET"/>' to your Manifest file if you
 * just copy this file. To get started, you should use the ChatWebSocketServer
 * from this Repository: https://bitbucket.org/pania/jetty-websocket-example/
 * 
 * @author pania
 * 
 */
public class MainActivity extends Activity {
	/**
	 * A tag for this Activity for logging purposes.
	 */
	private static final String TAG = "com.pania.websocketexample";

	static final int DIALOG_NO_CONNECTION_ID = 0;

	private final WebSocketConnection mConnection = new WebSocketConnection();

	/** Called when the user clicks the Send button */
	public void sendMessage(View view) {
		if (mConnection.isConnected()) {
			/** Get the entered message */
			TextView message = (TextView) findViewById(R.id.send_message);
			mConnection.sendTextMessage(message.getText().toString());
		} else {
			Log.d(TAG, "sendMessage called, but connection not active");
			showDialog(DIALOG_NO_CONNECTION_ID);
		}
	}

	private void start() {
		/** Set this variable to your own address! */
		final String wsuri = "ws://10.0.2.2:8081";

		try {
			mConnection.connect(wsuri, new WebSocketHandler() {

				/**
				 * This method will be called when a connection has been opened.
				 */
				@Override
				public void onOpen() {
					Log.d(TAG, "Status: Connected to " + wsuri);
				}

				/**
				 * This method will be called when a message is received.
				 */
				@Override
				public void onTextMessage(String payload) {
					Log.d(TAG, "Got echo: " + payload);
					// Display any received messages next to the 'Received'
					// TextView
					TextView resultText = (TextView) findViewById(R.id.result);
					resultText.setText(payload);
				}

				/**
				 * This method will be called when the connection has been
				 * closed.
				 */
				@Override
				public void onClose(int code, String reason) {
					Log.d(TAG, "Connection lost.");
				}
			});
		} catch (WebSocketException e) {
			Log.d(TAG, e.toString());
		}
	}

	protected Dialog onCreateDialog(int id) {
		Dialog dialog;
		switch (id) {
		case DIALOG_NO_CONNECTION_ID:
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(
					"There is no connection to the Server. Check your Server and the wsuri Parameter.")
					.setCancelable(false)
					.setNegativeButton("OK",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.cancel();
								}
							});
			AlertDialog alert = builder.create();
			dialog = alert;
			break;
		default:
			dialog = null;
		}
		return dialog;
	}

	/**
	 * This method creates the Activity and calls start() to setup the WebSocket
	 * Connection.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		start();
	}

}
